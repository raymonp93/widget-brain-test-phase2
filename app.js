var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const { handleError } = require('./helper/error');
var { myJokes } = require('./bin/www');

var indexRouter = require('./routes/index');
var jokesRouter = require('./routes/jokes');

var app = express();

app.use(logger('combined'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/jokes', jokesRouter);

// middleware to handle error
app.use((err, req, res, next) => {
    handleError(err, res);
});

module.exports = app;