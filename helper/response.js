class BaseResponse {
    
    constructor(data, status = 'success', statusCode = 200, message = 'Success') {
        this.data = data;
        this.status = status;
        this.statusCode = statusCode;
        this.message = message;
    }

}

module.exports = {
    BaseResponse
}