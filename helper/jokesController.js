const { save } = require('save-file');
const fs = require('fs');
const axios = require('axios');

async function saveJokes(jokes, fileName) {
    await save(fileName, JSON.stringify(jokes));
}

async function removeJokes(fileName) {
    fs.unlink(fileName, (err) => {
        if(err) {
            // error
            return false;
        }

        return true;
    });
}

async function getJokesAsyinc(fileName) {
    try {
        const data = await fs.promises.readFile(fileName, 'utf8');
        return JSON.parse(data);
      }
      catch(err) {
        // return empty array
        return [];
      }
  }

function getJokeFromRemote(remoteUrl, myJokes, promises) {
    promises.push(axios.get(remoteUrl)
        .then((response) => {
          let joke = myJokes.find(joke => joke.id === response.data.id);
          if(joke === undefined) {
            myJokes.push(response.data.value);
          }
      }));
}

module.exports = {
    saveJokes,
    getJokesAsyinc,
    getJokeFromRemote,
    removeJokes
}