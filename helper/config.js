const JokesConfig = {
    jokes_url: 'http://api.icndb.com/jokes/random/',
    jokes_count: 10,
}

module.exports = {
    JokesConfig
}