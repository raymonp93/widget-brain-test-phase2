var express = require('express');
var router = express.Router();
const { BusinessError } = require('../helper/error');
const { BaseResponse } = require('../helper/response');
const asyncHandler = require('express-async-handler');
const { saveJokes, getJokesAsyinc, removeJokes, getJokeFromRemote } = require('../helper/jokesController');
const { JokesConfig } = require('../helper/config');

var path = require('path');
var jokesFileName = path.join(__dirname, '..', 'data', 'myJokes.js');

router.get('/randomJokes', asyncHandler(async(req, res, next) => {
    let jokes = await getJokesAsyinc(jokesFileName);
    let result = shuffle(jokes).splice(0, 5);
    res.send(result);
}));

router.get('/newJokes', asyncHandler(async(req, res, next) => {
    let newJokes = [];
    let promises = [];

    for(i = 0; i<JokesConfig.jokes_count; i++) {
        getJokeFromRemote(JokesConfig.jokes_url, newJokes, promises);
    }

    await Promise.all(promises).then(response => {
        saveJokes(newJokes, jokesFileName);
    })

    res.send(new BaseResponse(newJokes));
}));

router.delete('/removeAll', asyncHandler(async(req, res, next) => {
    let failed = await removeJokes(jokesFileName);

    if(!failed) {
        res.send(new BaseResponse([]));
    } else {
        throw new BusinessError(400, 'Failed to delete file...');
    }
}));

router.get('/jokesSummary', asyncHandler(async(req, res, next) => {
    let wordCount = {};
    let myJokes = await getJokesAsyinc(jokesFileName);

    myJokes.forEach(j => {
        let words = j.joke.split(" ");
        words.forEach(w => {
            if(wordCount[w] === undefined) {
                wordCount[w] = 1;
            } else {
                // assume it is numbers
                wordCount[w] = wordCount[w]+1;
            }
        })
    });

    let response = {
        jokes : [...myJokes.map( j => j.joke)],
        words : wordCount
    }

    res.send(new BaseResponse(response));
}));

function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
  
    // While there remain elements to shuffle.
    while (0 !== currentIndex) {
  
      // Pick a remaining element.
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
  
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
  
    return array;
};

module.exports = router;
