### WidgetBrain Technical Test Phase 2

default port is 3000

default jokes url is http://api.icndb.com/jokes/random

default jokes storage is ../data/myJokes.js

### How to Run

- `npm install`


- `npm run start`


### Route List

-  GET - localhost:$PORT/jokes/randomJokes
-  GET - localhost:$PORT/jokes/newJokes
-  GET - localhost:$PORT/jokes/jokesSummary
-  GET - localhost:$PORT/jokes/removeAll